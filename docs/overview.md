# Overview

## Description

This gear imports header information of file as Flywheel metadata. The metadata are
stored as Custom Information and fully indexed by Flywheel. Control of the metadata
stored is given to the user through configuration options and a variety of file types is
supported.

## Input

The gear takes a single file as input. The following file types are currently supported:

* DICOM and DICOM Zip Archive
* PTD (Siemens PT format)
* NIfTI
* ParaVision (Bruker format)
* PAR/REC (Philips format) as a single .PAR file or as a zipped .parrec.zip file
* EEG (BrainVision, EDF, BDF, EEGLAB)

## Output

The output consists of the extracted metadata, stored on the input file Custom
Information under the `header` namespace.

## Classification
<!-- markdownlint-disable MD032 -->
* Maintainer: <img src="https://img.shields.io/badge/Flywheel-1B68FA" alt="Flywheel">  
* Therapeutic Area: <img src="https://img.shields.io/badge/Any-1B68FA" alt="Any">
* Modality: <img src="https://img.shields.io/badge/Any-1B68FA" alt="Any">  
* Suite: <img src="https://img.shields.io/badge/Utility-1B68FA" alt="Utility">  
* File Type:
<img src="https://img.shields.io/badge/dicom-1B68FA" alt="dicom">
<img src="https://img.shields.io/badge/nifti-1B68FA" alt="nfiti">
<img src="https://img.shields.io/badge/ParaVision-1B68FA" alt="ParaVision">
<img src="https://img.shields.io/badge/parerec-1B68FA" alt="parrec">
<img src="https://img.shields.io/badge/eeg-1B68FA" alt="eeg">
* GPU: <img src="https://img.shields.io/badge/No-1B68FA" alt="No">
<!-- markdownlint-enable MD032 -->
