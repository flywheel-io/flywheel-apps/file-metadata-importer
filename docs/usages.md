# Usages and Workflows

The file-metadata-importer gear is designed to help you import and manage metadata for
your files so that they can be indexed in Flywheel. The gear can be used in a variety of
ways, depending on your specific needs and requirements. This section will cover the
different usages and workflows of the gear,

## Synergy with other gears

The file-metadata-importer gear is part of suite of gears that are designed to work
together to help you curate and manage your files. For instance, the
file-metadata-importer gear can be used in conjunction with
[file-classifier](https://gitlab.com/flywheel-io/scientific-solutions/gears/file-classifier)
gear to classify files based on their metadata.

## DataView and Advanced Search

The `file-metadata-importer` gear can be used in conjunction with DataView and Advanced
Search to build cohorts or to characterize your dataset based on specific metadata.

For instance, you can easily build the following DataView to report on `Manufacturer`
and `ManufacturerModelName` tags:

![alt text](assets/images/dataview.png)

## Gear Rules

The `file-metadata-importer` gear is also a good candidate to be run as [Gear
Rule](https://docs.flywheel.io/user/compute/gears/user_gear_rules/) automatically on file
upload so that the header metadata can be extracted and indexed in Flywheel as soon as
the file is uploaded. To do so, you can create a gear rule that triggers the
`file-metadata-importer` gear on file upload as shown below:

![alt text](assets/images/gear-rules.png)
