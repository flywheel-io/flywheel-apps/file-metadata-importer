asttokens==2.4.1 ; python_version >= "3.10" and python_version < "4.0"
atomicwrites==1.4.1 ; python_version >= "3.10" and python_version < "4.0" and sys_platform == "win32"
babel==2.16.0 ; python_version >= "3.10" and python_version < "4.0"
click==8.1.7 ; python_version >= "3.10" and python_version < "4.0"
colorama==0.4.6 ; python_version >= "3.10" and python_version < "4.0"
coverage==5.5 ; python_version >= "3.10" and python_version < "4"
coverage[toml]==5.5 ; python_version >= "3.10" and python_version < "4"
csscompressor==0.9.5 ; python_version >= "3.10" and python_version < "4.0"
exceptiongroup==1.2.2 ; python_version >= "3.10" and python_version < "3.11"
executing==2.0.1 ; python_version >= "3.10" and python_version < "4.0"
ghp-import==2.1.0 ; python_version >= "3.10" and python_version < "4.0"
griffe==0.49.0 ; python_version >= "3.10" and python_version < "4.0"
htmlmin2==0.1.13 ; python_version >= "3.10" and python_version < "4.0"
iniconfig==2.0.0 ; python_version >= "3.10" and python_version < "4.0"
ipython==8.26.0 ; python_version >= "3.10" and python_version < "4.0"
jedi==0.19.1 ; python_version >= "3.10" and python_version < "4.0"
jsmin==3.0.1 ; python_version >= "3.10" and python_version < "4.0"
markdown==3.7 ; python_version >= "3.10" and python_version < "4.0"
matplotlib-inline==0.1.7 ; python_version >= "3.10" and python_version < "4.0"
mdx-truly-sane-lists==1.3 ; python_version >= "3.10" and python_version < "4.0"
mergedeep==1.3.4 ; python_version >= "3.10" and python_version < "4.0"
mkdocs-autorefs==1.1.0 ; python_version >= "3.10" and python_version < "4.0"
mkdocs-gen-files==0.5.0 ; python_version >= "3.10" and python_version < "4.0"
mkdocs-get-deps==0.2.0 ; python_version >= "3.10" and python_version < "4.0"
mkdocs-glightbox==0.3.7 ; python_version >= "3.10" and python_version < "4.0"
mkdocs-literate-nav==0.6.1 ; python_version >= "3.10" and python_version < "4.0"
mkdocs-material-extensions==1.3.1 ; python_version >= "3.10" and python_version < "4.0"
mkdocs-material==9.5.33 ; python_version >= "3.10" and python_version < "4.0"
mkdocs-minify-plugin==0.8.0 ; python_version >= "3.10" and python_version < "4.0"
mkdocs==1.6.0 ; python_version >= "3.10" and python_version < "4.0"
mkdocstrings-python==1.10.0 ; python_version >= "3.10" and python_version < "4.0"
mkdocstrings==0.24.3 ; python_version >= "3.10" and python_version < "4.0"
paginate==0.5.7 ; python_version >= "3.10" and python_version < "4.0"
parso==0.8.4 ; python_version >= "3.10" and python_version < "4.0"
pathspec==0.12.1 ; python_version >= "3.10" and python_version < "4.0"
pexpect==4.9.0 ; python_version >= "3.10" and python_version < "4.0" and (sys_platform != "win32" and sys_platform != "emscripten")
pluggy==1.5.0 ; python_version >= "3.10" and python_version < "4.0"
prompt-toolkit==3.0.47 ; python_version >= "3.10" and python_version < "4.0"
ptyprocess==0.7.0 ; python_version >= "3.10" and python_version < "4.0" and (sys_platform != "win32" and sys_platform != "emscripten")
pure-eval==0.2.3 ; python_version >= "3.10" and python_version < "4.0"
py==1.11.0 ; python_version >= "3.10" and python_version < "4.0"
pygments==2.18.0 ; python_version >= "3.10" and python_version < "4.0"
pymdown-extensions==10.9 ; python_version >= "3.10" and python_version < "4.0"
pytest-cov==2.12.1 ; python_version >= "3.10" and python_version < "4.0"
pytest-mock==3.14.0 ; python_version >= "3.10" and python_version < "4.0"
pytest==6.2.5 ; python_version >= "3.10" and python_version < "4.0"
pyyaml-env-tag==0.1 ; python_version >= "3.10" and python_version < "4.0"
regex==2024.7.24 ; python_version >= "3.10" and python_version < "4.0"
stack-data==0.6.3 ; python_version >= "3.10" and python_version < "4.0"
toml==0.10.2 ; python_version >= "3.10" and python_version < "4.0"
traitlets==5.14.3 ; python_version >= "3.10" and python_version < "4.0"
watchdog==5.0.0 ; python_version >= "3.10" and python_version < "4.0"
wcwidth==0.2.13 ; python_version >= "3.10" and python_version < "4.0"
