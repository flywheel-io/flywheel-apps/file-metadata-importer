from fw_gear_file_metadata_importer.derived import compute_scan_coverage


def test_compute_scan_coverage():
    z_array = [0.0, 1.0, 2.0]
    scan_coverage, max_sl, min_sl = compute_scan_coverage(z_array)

    assert scan_coverage == 2.0
    assert max_sl == 2.0
    assert min_sl == 0.0
