from contextlib import nullcontext as does_not_raise

import flywheel
import pytest
from ruamel import yaml

from fw_gear_file_metadata_importer.main import run
from tests.conftest import DICOM_ROOT


def generate_yaml_dump():
    """Use to generate yaml dump of the test files"""
    FILENAMES = ["archive.dicom.zip", "t1_sample_fixed.dcm"]
    project = flywheel.Project(label="Test", info={})
    for filename in FILENAMES:
        file_type = "dicom"
        file_path = DICOM_ROOT / filename

        fe, meta, qc = run(file_type, file_path, project=project)

        with open(DICOM_ROOT / f"{filename}_dump.yaml", "w") as fp:
            yaml.safe_dump(fe, fp)

        with open(DICOM_ROOT / f"{filename}_meta.yaml", "w") as fp:
            yaml.safe_dump(dict(meta), fp)


@pytest.mark.parametrize("filename", ["archive.dicom.zip", "t1_sample_fixed.dcm"])
def test_run(dicom_file, filename):
    """See generate_yaml_dump to generates this dumps."""
    project = flywheel.Project(label="Test", info={})
    file_path = dicom_file(filename)
    file_type = "dicom"

    fe, meta, qc = run(file_type, file_path, project=project)

    with open(DICOM_ROOT / f"{filename}_dump.yaml", "r") as fp:
        res = yaml.safe_load(fp)
    assert res == fe

    with open(DICOM_ROOT / f"{filename}_meta.yaml", "r") as fp:
        res = yaml.safe_load(fp)
    # cleaning timezone
    for k in list(res.keys()):
        # because timestamp and timezone are based on local zoned
        if k.endswith("timezone") or k.endswith("timestamp"):
            del res[k]
            del meta[k]

    assert res == meta


# def test_run_fails_on_invalid_file(dicom_file):
#    """See generate_yaml_dump to generates this dumps."""
#    project = flywheel.Project(label="Test", info={})
#    file_path = dicom_file("t1_sample.dcm")
#    file_type = "dicom"
#    with pytest.raises(SystemExit):
#        _ = run(file_type, file_path, project=project)


def test_run_log_error_if_unk_type(dicom_file, caplog):
    file_type = "unk"
    project = flywheel.Project(label="Test", info={})
    file_path = dicom_file("archive.dicom.zip")

    with pytest.raises(SystemExit) as err:
        _ = run(file_type, file_path, project=project)

    assert err.type is SystemExit
    assert err.value.code == 1
    assert "File type unk is not supported" in caplog.messages[0]


@pytest.mark.parametrize(
    "file_type,file_path,raises",
    [
        ("dicom", "", does_not_raise()),
        ("ptd", "", does_not_raise()),
        ("ParaVision", "", does_not_raise()),
        ("parrec", "", does_not_raise()),
        (None, "test.ptd", does_not_raise()),
        (None, "", pytest.raises(SystemExit)),
        ("test_type", "", pytest.raises(SystemExit)),
    ],
)
def test_run_multiple_file_types(mocker, file_type, file_path, raises):
    mocker.patch("fw_gear_file_metadata_importer.main.project_tag_update")
    pef = mocker.patch(
        "fw_gear_file_metadata_importer.files.bruker.paravis_extract_files"
    )
    pef.return_value = "acqp", "method"
    process_mocks = []
    process_mocks.append(
        mocker.patch("fw_gear_file_metadata_importer.files.dicom.process")
    )
    process_mocks.append(
        mocker.patch("fw_gear_file_metadata_importer.files.siemens.process_ptd")
    )
    process_mocks.append(
        mocker.patch("fw_gear_file_metadata_importer.files.bruker.process")
    )
    process_mocks.append(
        mocker.patch("fw_gear_file_metadata_importer.files.philips.process")
    )
    for mock in process_mocks:
        mock.return_value = ("test",) * 3

    with raises:
        run(file_type, file_path, None, False)
