from fw_gear_file_metadata_importer.files.siemens import process_ptd


def test_process_ptd_valid(mocker):
    ptd_mock = mocker.patch("fw_gear_file_metadata_importer.files.siemens.PTDFile")
    header = mocker.patch(
        "fw_gear_file_metadata_importer.files.dicom.get_file_info_header"
    )
    qc = mocker.patch("fw_gear_file_metadata_importer.files.dicom.get_file_qc")

    ptd_mock.return_value.preamble = b"test"
    header.return_value = {"dicom": {"test": 1}}
    qc.return_value = {"test2": 2}

    fe, meta, qc_val = process_ptd("test")

    ptd_mock.assert_called_once_with("test")
    header.assert_called_once_with(ptd_mock.return_value.dcm, None, siemens_csa=False)
    qc.assert_called_once_with(
        ptd_mock.return_value.dcm, ptd_mock.return_value.filepath
    )

    assert fe == {
        "modality": ptd_mock.return_value.dcm.get.return_value,
        "info": {"header": {"dicom": {"test": 1}, "ptd": "test"}},
    }
    assert meta == ptd_mock.return_value.dcm.get_meta()
    assert qc_val == {"test2": 2}


def test_process_ptd_raw_header(mocker):
    ptd_mock = mocker.patch("fw_gear_file_metadata_importer.files.siemens.PTDFile")
    header = mocker.patch(
        "fw_gear_file_metadata_importer.files.dicom.get_file_info_header"
    )
    qc = mocker.patch("fw_gear_file_metadata_importer.files.dicom.get_file_qc")

    ptd_mock.return_value.preamble = b"\xcf\xa0\xeb"
    header.return_value = {"dicom": {"test": 1}}
    qc.return_value = {"test2": 2}

    fe, meta, qc_val = process_ptd("test")

    ptd_mock.assert_called_once_with("test")
    header.assert_called_once_with(ptd_mock.return_value.dcm, None, siemens_csa=False)
    qc.assert_called_once_with(
        ptd_mock.return_value.dcm, ptd_mock.return_value.filepath
    )

    # PTD should not be in info
    assert fe == {
        "modality": ptd_mock.return_value.dcm.get.return_value,
        "info": {"header": {"dicom": {"test": 1}}},
    }
    assert meta == ptd_mock.return_value.dcm.get_meta()
    assert qc_val == {"test2": 2}
