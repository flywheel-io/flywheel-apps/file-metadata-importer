import json
import tempfile
from pathlib import Path
from unittest import mock

import flywheel
import pytest
from flywheel_gear_toolkit import GearToolkitContext
from pydicom.tag import Tag

from fw_gear_file_metadata_importer.util import (
    archived_filetype,
    create_metadata,
    format_address,
    get_startswith_lstrip_dict,
    load_p15e_tags_from_file,
    remove_empty_values,
    sanitize_modality,
    validate_file,
    validate_file_size,
)
from tests.conftest import ASSETS_ROOT


def test_get_startswith_dict_return_filtered_dict():
    dict_ = {"bla.toto": None, "bli.titi": None}
    res = get_startswith_lstrip_dict(dict_, "bla")
    assert res == {"toto": None}


def test_validate_file_size_reports_when_file_is_empty():
    with tempfile.NamedTemporaryFile() as fp:
        err = validate_file_size(fp.name)
        assert len(err) == 1
        assert err[0].startswith("File is empty")


def test_validate_file_size_when_file_is_not_empty(dicom_file):
    file = dicom_file("siemens_with_csa.dcm")
    err = validate_file_size(file)
    assert len(err) == 0


def test_validate_file():
    validate_file_size = mock.MagicMock(return_value=["Some Error"])
    with mock.patch(
        "fw_gear_file_metadata_importer.util.validate_file_size", validate_file_size
    ):
        err = validate_file(tempfile.NamedTemporaryFile().name)
    assert err == ["Some Error"]


@pytest.mark.parametrize(
    "fe",
    [
        flywheel.FileEntry(
            modality="MR", info={"header": {"dicom": {"PatientID": "Yo"}}}
        ),
        flywheel.FileEntry(info={"header": {"dicom": {"PatientID": "Yo"}}}),
    ],
)
def test_create_metadata(tmpdir, fe):
    with GearToolkitContext(
        gear_path=tmpdir,
        config_path=ASSETS_ROOT / "config.json",
        input_args=[],
        manifest_path=ASSETS_ROOT.parents[1] / "manifest.json",
    ) as context:
        context.get_destination_container = mock.MagicMock()
        meta = {"session.label": "Plop", "acquisition.label": "Plip"}
        qc = {
            "filename": "1.2.840.113619.2.475.82.MR.dcm",
            "trace": ["Tag (0020, 0032) Replace VR: DS -> OB"],
        }

        create_metadata(context, fe, meta, qc)

    with open(Path(tmpdir) / "output" / ".metadata.json", "r") as fp:
        metadata = json.load(fp)

    assert metadata["session"]["label"] == "Plop"
    assert metadata["acquisition"]["label"] == "Plip"
    info = metadata["acquisition"]["files"][0]["info"]
    assert info["header"] == {"dicom": {"PatientID": "Yo"}}
    assert "Iam" in info
    qc = info["qc"]["file-metadata-importer"]
    result = qc["metadata-extraction"]
    assert result["state"] == "PASS"
    assert result["data"]["filename"] == "1.2.840.113619.2.475.82.MR.dcm"
    assert result["data"]["trace"] == ["Tag (0020, 0032) Replace VR: DS -> OB"]


@pytest.mark.parametrize(
    "source,res",
    [
        ({"a": None}, {}),
        ({"a": []}, {}),
        ({"a": {}}, {}),
        ({"a": ""}, {}),
        ({"a": 0}, {"a": 0}),
        ({"a": {"b": 0, "c": ""}}, {"a": {"b": 0}}),
        (
            {"my_val": {"b": Tag((0x0020, 0x0020))}},
            {"my_val": {"b": Tag((0x0020, 0x0020))}},
        ),
    ],
)
def test_remove_empty_values(source, res):
    r = remove_empty_values(source)
    assert r == res


def test_sanitize_modality():
    res = sanitize_modality("PET/CT")
    assert res == "PET-CT"


def test_archived_filetype():
    assert archived_filetype(ASSETS_ROOT / "EEG/brainvision.zip")
    assert archived_filetype(ASSETS_ROOT / "EEG/eeglab.zip")


@mock.patch("zipfile.ZipFile.namelist")
def test_archived_filepath(mock_namelist):
    mock_namelist.return_value = ["test.vhdr", "test.set"]
    assert not archived_filetype(ASSETS_ROOT / "EEG/brainvision.zip")


def test_load_p15e_tags_from_file():
    mock_format_address = mock.MagicMock(return_value="1234xx01")
    with mock.patch(
        "fw_gear_file_metadata_importer.util.format_address", mock_format_address
    ):
        mock_format_address.return_value = "1234xx01"
        tags = load_p15e_tags_from_file()
        mock_format_address.assert_called()
        test_tag = tags.pop()
        assert test_tag[-1] == "1234xx01"


def test_format_address():
    result = "1234xx01"

    address = " ( 1234, ab01 ) "
    formatted_address = format_address(address)
    assert formatted_address == result

    address = "1234ab01"
    formatted_address = format_address(address)
    assert formatted_address == result

    address = "1234,ab01"
    formatted_address = format_address(address)
    assert formatted_address == result
